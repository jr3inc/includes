#include "TimeDate.h"

//default constructor
//2010Nov15 ka: written and tested
TimeDate::TimeDate(void)
{
	now();		//sets time to now
}

//constructor with all parameters
//2010Nov15 ka: written and tested
//NOTE: does not handle bad data gracefully
TimeDate::TimeDate(int yr, int mo, int dy, int hr, int min, int sec)
{
	year = yr;
	month = mo;
	day = dy;
	hour = hr;
	minute = min;
	second = sec;
}


TimeDate::~TimeDate(void)
{
}


//validates data in class
//2010Nov15 ka: written
//UNTESTED
bool TimeDate::isValid()
{
	bool isGood, leapYear;
	int daysInMonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	isGood = (second >= 0) && (minute >= 0) && (hour >= 0) && (day > 0) &&
		(month > 0) && (year >= 1901);

	isGood = isGood && (second < 60) && (minute < 60) && (hour < 24) && (month <= 12);

	leapYear = ((year % 4) == 0);
	if (leapYear)
		daysInMonth[1]++;
	isGood = isGood && (day <= daysInMonth[month-1]);

	return isGood;
}

//returns month, day and year formatted as ddMMMyyyy with leading 0 if needed
//2010Nov15 ka: written and tested
string TimeDate::getMDY()
{
	string mdy;
	stringstream strm;

	strm << day;
	mdy = strm.str();
	strm.str("");		//clear out stream
	while (mdy.length() < 2)
		mdy.insert(0, "0");

	switch (month)
	{
	case 1:
		mdy += "JAN";
		break;
	case 2:
		mdy += "FEB";
		break;
	case 3:
		mdy += "MAR";
		break;
	case 4:
		mdy += "APR";
		break;
	case 5:
		mdy += "MAY";
		break;
	case 6:
		mdy += "JUN";
		break;
	case 7:
		mdy += "JUL";
		break;
	case 8:
		mdy += "AUG";
		break;
	case 9:
		mdy += "SEP";
		break;
	case 10:
		mdy += "OCT";
		break;
	case 11:
		mdy += "NOV";
		break;
	case 12:
		mdy += "DEC";
	};

	strm << year;
	mdy += strm.str();

	return mdy;
}


string TimeDate::getHMS()
{//STUB
	string hms, tempStr;
	stringstream strm;

	strm << hour;
	hms = strm.str() + ":";
	strm.str("");		//clear out stream
	while (hms.length() < 3)
		hms.insert(0, "0");

	strm << minute;
	tempStr = strm.str() + ":";
	strm.str("");		//clear out stream
	while (tempStr.length() < 3)
		tempStr.insert(0, "0");
	hms += tempStr;

	strm << second;
	tempStr = strm.str();
	while (tempStr.length() < 2)
		tempStr.insert(0, "0");
	hms += tempStr;
	return hms;
}

//sets time to current system time
//2010Nov15 ka: written and tested
bool TimeDate::now()
{
	bool isGood = true;
	time_t sysSeconds;
	struct tm * timeinfo;

	sysSeconds = time(NULL);
	if (sysSeconds < 0)
		isGood = false;
	else
	{
		timeinfo = localtime(&sysSeconds);
		year = timeinfo->tm_year + 1900;
		month = timeinfo->tm_mon + 1;
		day = timeinfo->tm_mday;
		hour = timeinfo->tm_hour;
		minute = timeinfo->tm_min;
		second = timeinfo->tm_sec;
	};

	return isGood;
}

//parses a DTW string or a string in ddmmmyyy tab hh:mm:ss format
bool TimeDate::parse(string dtwString)
{//STUB
	bool isGood = true;
	string tempStr;
	size_t foundPos;

	tempStr = dtwString.substr(0,3);
	if (tempStr == "DTW" || tempStr == "dtw")
	{
		foundPos = dtwString.find_first_of("\t");
		if (foundPos != string::npos)
			dtwString.erase(0, foundPos + 1);
		else
			dtwString.erase(0, 3);
	};

	foundPos = dtwString.find_first_of("\t");
	if (foundPos != string::npos)
	{
		tempStr = dtwString.substr(0, foundPos);
		isGood = parseDMY(tempStr);
		if (isGood)
			dtwString.erase(0, foundPos + 1);
	}
	else
		isGood = false;

	if (isGood)
	{
		foundPos = dtwString.find_first_of("\t");
		if (foundPos != string::npos)
		{
			tempStr = dtwString.substr(0, foundPos);
			isGood = parseHMS(tempStr);
		}
		else
			isGood = false;
	}

	return isGood;
}

//parses dmy string. month MUST be three alpha characters, year must be 4 digits
//2010Nov15 ka: written and tested
bool TimeDate::parseDMY(string dmyString)
{
	bool isGood = true;
	string tempStr;
	size_t foundPos;

	foundPos = dmyString.find_first_not_of("0123456789");
	if (foundPos != string::npos)
	{
		tempStr = dmyString.substr(0, foundPos);
		dmyString.erase(0, foundPos);
		this->day = atoi(tempStr.c_str());
	}
	else
		isGood = false;

	if (isGood)
	{
		tempStr = dmyString.substr(0,3);
		this->month = stringToMonth(tempStr);
		dmyString.erase(0,3);
	};

	if (isGood)
		this->year = atoi(dmyString.c_str());

	isGood = isGood && this->isValid();

	return isGood;
}


//parses a string in the format hh:mm:ss
//delimiter can be an non-digit
//2010Nov15 ka: written and tested
bool TimeDate::parseHMS(string hmsString)
{//STUB
	bool isGood = true;
	string tempStr;
	size_t foundPos;

	foundPos = hmsString.find_first_not_of("0123456789");
	if (foundPos != string::npos)
	{
		tempStr = hmsString.substr(0, foundPos);
		hmsString.erase(0, foundPos + 1);
		this->hour = atoi(tempStr.c_str());
	}
	else
		isGood = false;
	
	if (isGood)
	{
		foundPos = hmsString.find_first_not_of("0123456789");
		if (foundPos != string::npos)
		{
			tempStr = hmsString.substr(0, foundPos);
			hmsString.erase(0, foundPos + 1);
			this->minute = atoi(tempStr.c_str());
		}
		else
			isGood = false;
	};

	if (isGood)
	{
		this->second = atoi(hmsString.c_str());
		if (this->second < 0 || this->second >= 60)
			isGood = false;
	};
	return isGood;
}

//takes a string and coverts to month number
//2010Nov15 ka: written and tested
int TimeDate::stringToMonth(string monthStr)
{
	int monthNum;
	unsigned i;
	string tempStr;

	tempStr = monthStr.substr(0,3);
	for (i = 0; i < tempStr.length(); i++)
		tempStr[i] = tolower(tempStr[i]);

	monthNum = -1;			//if we don't find a valid month, return -1

	if (tempStr == "jan")
		monthNum = 1;
	if (tempStr == "feb")
		monthNum = 2;
	if (tempStr == "mar")
		monthNum = 3;
	if (tempStr == "apr")
		monthNum = 4;
	if (tempStr == "may")
		monthNum = 5;
	if (tempStr == "jun")
		monthNum = 6;
	if (tempStr == "jul")
		monthNum = 7;
	if (tempStr == "aug")
		monthNum = 8;
	if (tempStr == "sep")
		monthNum = 9;
	if (tempStr == "oct")
		monthNum = 10;
	if (tempStr == "nov")
		monthNum = 11;
	if (tempStr == "dec")
		monthNum = 12;
	
	return monthNum;
}

string TimeDate::getDT()
{
	string dtwString;
	const string TAB = "\t";

	dtwString = "DTW" + TAB + this->getMDY() + TAB + this->getHMS();
	return dtwString;
}