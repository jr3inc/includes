#pragma warning(disable : 4996)		//get rid of warnings about deprecated code that only Microsoft has deprecated
#ifndef TimeDate_H
#define TimeDate_H

#include <ctime>
#include <string>
#include <sstream>
#include <iostream>
#include <cstdlib>


class TimeDate
{
public:
	TimeDate(void);
	TimeDate(int, int, int, int, int, int);
	~TimeDate(void);

	bool isValid();

	//get methods
	int getYear() {return year;};
	int getMonth() {return month;};
	int getDay() {return day;};
	int getHour() {return hour;};
	int getMinute() {return minute;};
	int getSecond() {return second;};
	std::string getMDY();				//returns month-day-year in ddMMMyyyy format
	std::string getHMS();				//returns hour-minute-second in HH:MM:SS format
	std::string getDT();				//returns a DTW string minus the who
	int getMinutesSince(TimeDate);		//returns the number of minutes between now and the parameter
	int getMinutesSince2000();			//returns the number of minutes between TimeDate and Jan 1, 2000
	std::string secondsToMinutes(int, int);	//returns the number of minutes represented by the first parameter
										//formatted to the number of decimal places in the second parameter

	//set methods
	bool now();						//sets time to current time
	bool parse(std::string);			//parses from a DTW string
	bool parseDMY(std::string);
	bool parseHMS(std::string);
	int stringToMonth(std::string);	//takes in a string and returns the integer month

	void delay(int);

protected:
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;
};

#endif

