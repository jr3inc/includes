//***********************************************************************
// Project:		Used to get system time. Format in different types.
//				The time is that off the clock. (*as in the one in the system try*)
// By:			Brian Ramming
// Description:	Grabs local system time: hour, minute, second, day
//				month, year. Returns each as either an int. or a string.
//************************************************************************


#ifndef TIMEDATE_H

#define TIMEDATE_H
using namespace std;


#pragma warning(disable : 4996)  //disable the deprecated warnings (only deprecated in VS)

#include <Windows.h>
#include <sstream>
#include <time.h>
#include <fstream>

class Time
{
public:

	string intToString(int num)
	{
		ostringstream _int;
		_int << num << flush;
		return _int.str();
	}

	/*
	 * Returns the current hour as INT
	 */
	int hour()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return times->tm_hour;
	}

	/*
	 * Returns the current minute as INT
	 */
	int minute()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return times->tm_min;
	}
	
	/*
	 * Returns the current second as INT
	 */
	int second()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return times->tm_sec;
	}
	
	/*
	 * Returns the current year as INT
	 */
	int year()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return times->tm_year+1900;
	}
	
	/*
	 * Returns the current month as INT
	 */
	int month()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return times->tm_mon;
	}
	
	/*
	 * Returns the current day as INT
	 */
	int day()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return times->tm_mday;
	}

	/*
	 * Returns the current hour as string.
	 */
	string sHour()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return intToString(times->tm_hour);
	}

	/*
	 * Returns the current minute as string.
	 */
	string sMinute()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return intToString(times->tm_min);
	}

	/*
	 * Returns the current second as string.
	 */
	string sSecond()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return intToString(times->tm_sec);
	}
	
	/*
	 * Returns the current year as string.
	 */
	string sYear()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return intToString(times->tm_year + 1900);
	}

	/*
	 * Returns the current day as string.
	 */
	string sDay()
	{
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		return intToString(times->tm_mday);
	}

	
	/*
	 * Returns the current mont as THREE letter string
	 */
	string sMonth()
	{
		string month;
		struct tm *times;			//sets up struct to hold time data.
		time_t long_time;			//Time data is year, month, hour, min
	
		time(&long_time);
		times = localtime(&long_time);

		switch(times->tm_mon)
		{
		case 0: month = "Jan"; break;
		case 1: month = "Feb"; break;
		case 2: month = "Mar"; break;
		case 3: month = "Apr"; break;
		case 4: month = "May"; break;
		case 5: month = "Jun"; break;
		case 6: month = "Jul"; break;
		case 7: month = "Aug"; break;
		case 8: month = "Sep"; break;
		case 9: month = "Oct"; break;
		case 10: month = "Nov"; break;
		case 11: month = "Dec"; break;
		default: month = "Invalid"; break;
		}
		return month;
	}

	int stringMonthToIntMonth(string month)
	{
		int monthINT = -1;
		if(month == "Jan") monthINT = 0;
		else if(month == "Feb") monthINT = 1;
		else if(month == "Mar") monthINT = 2;
		else if(month == "Apr") monthINT = 3;
		else if(month == "May") monthINT = 4;
		else if(month == "Jun") monthINT = 5;
		else if(month == "Jul") monthINT = 6;
		else if(month == "Aug") monthINT = 7;
		else if(month == "Sep") monthINT = 8;
		else if(month == "Oct") monthINT = 9;
		else if(month == "Nov") monthINT = 10;
		else if(month == "Dec") monthINT = 11;

		return monthINT;
	}

	string hourMinSec()
	{
		return sHour() + ":" + sMinute() + ":" + sSecond();
	}

	string date()
	{
		string theDate;
		theDate = sDay() + sMonth() + sYear();
		return theDate;
	}

	void delay(int milliseconds)
	{
		int h;
		clock_t start_clock = clock();
		h = clock();
		while(clock() < h + milliseconds);
	}

	int getDaysInMonth(int month)
	{
		int days = 0;
		switch(month)
		{
		case 0: 
		case 2: 
		case 4:
		case 6:
		case 7:
		case 9:
		case 11: days = 31; break;
		case 1: days = 28; break;
		case 3:
		case 5:
		case 8:
		case 10: days = 30; break;
		}
		return days;
	}

	int DaysToDate(int dDay, int dMonth, int dYear)
	{
		int numberOfDays = 0, daysInMonth = 0, monthCheck, tempMonth;

		int currentDay = day();
		int currentMonth = month();
		int currentYear = year();

		if(currentMonth == dMonth)
			numberOfDays = dDay - currentDay;
		else
		{
			tempMonth = currentMonth;
			daysInMonth = getDaysInMonth(currentMonth); //this will change to a swtich statement depending on what month it is.
			numberOfDays = daysInMonth - currentDay;
			if(dMonth-1 < 0)
				monthCheck = 11;
			else
				monthCheck = dMonth - 1;
			while(currentMonth != monthCheck)
			{
				if(tempMonth == 11) tempMonth = 0;
				else tempMonth++;
				daysInMonth = getDaysInMonth(tempMonth);
				numberOfDays = numberOfDays + daysInMonth;
				if(monthCheck-1 < 0)
					monthCheck = 11;
				else
					monthCheck = monthCheck - 1;
			}
			numberOfDays = numberOfDays + dDay;
		}
		return numberOfDays;
	}

	//************************************
	// These next methods are used to get the time from the DTWFile
	// that maintains the time from the computer in the break room.
	//***********************************

	int getJR3hour()
	{
		int jr3hour;
		fstream timeFile;
		string date; 
	
		timeFile.open("Z:\\DTWFile.txt", ios::in);
		timeFile>>date;
		timeFile>>jr3hour;

		timeFile.close();
		return jr3hour;
	}

	int getJR3min()
	{
		int jr3min, t1;
		fstream timeFile;
		string date; 
	
		timeFile.open("Z:\\DTWFile.txt", ios::in);
		timeFile>>date;
		timeFile>>t1; timeFile.ignore(); timeFile>>jr3min;

		timeFile.close();
		return jr3min;
	}

	int getJR3sec()
	{
		int jr3sec, t1, t2;
		fstream timeFile;
		string date; 
	
		timeFile.open("Z:\\DTWFile.txt", ios::in);
		timeFile>>date;
		timeFile>>t1; timeFile.ignore();
		timeFile>>t2;  timeFile.ignore(); timeFile>>jr3sec;

		timeFile.close();
	
		return jr3sec;
	}

/////////////

	string getDTW()
	{
		fstream timeFile;
		string dtw;

		timeFile.open("Z:\\DTWFile.txt", ios::in);
		getline(timeFile, dtw, '\n');

		timeFile.close();

		return dtw;
	}
};

#endif