//Doxygen commenting
/**
* @file JR3_PCI.h
* @brief sensor data logger
* @author  JR3 Inc <jr3@jr3.com> <http://www.jr3.com>
* @version 1.0
* @date 2010
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 3 of
* the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details at
* http://www.gnu.org/licenses/lgpl-3.0.txt
*
* @section DESCRIPTION
*
* The time class provides data logging facilities for JR3 digital sensors.
*/
#ifndef JR3_PCI_H
#define JR3_PCI_H

#include <string>

namespace jr3{
        /*! @class Jr3_PCI
        *  @brief Class to handle JR3 PCI card
        *  @author Kathy Applebaum
        *  @version 0.1
        *  @date 2011
        */
        // To use class, first call init, then call offsetInit. Besure to call close at end.
        class Jr3_PCI
        {
        public:
                //constructors
                Jr3_PCI(void);
                Jr3_PCI(string);

                //accessors
                int getSerialNumber(short whichProcessor = 0);
                int getCounts(short, short = 0);
                int getClock(short whichProcessor = 0);
                int getRaw(short, short, int);
                int getFxCounts(short whichProcessor = 0) {return getCounts(0, whichProcessor);};
                int getFyCounts(short whichProcessor = 0) {return getCounts(1, whichProcessor);};
                int getFzCounts(short whichProcessor = 0) {return getCounts(2, whichProcessor);};
                int getMxCounts(short whichProcessor = 0) {return getCounts(3, whichProcessor);};
                int getMyCounts(short whichProcessor = 0) {return getCounts(4, whichProcessor);};
                int getMzCounts(short whichProcessor = 0) {return getCounts(5, whichProcessor);};

                std::string getAllCounts(short whichProcessor = 0);
                std::string getSixChannel(short whichProcessor = 0);
                std::string getEightChannel(short whichProcessor = 0);

                int getFxFilter(unsigned short whichFilter, short whichProcessor = 0)
                        {return getFilter(FX_OFFSET, whichFilter, whichProcessor);};
                int getFyFilter(unsigned short whichFilter, short whichProcessor = 0)
                        {return getFilter(FY_OFFSET, whichFilter, whichProcessor);};
                int getFzFilter(unsigned short whichFilter, short whichProcessor = 0)
                        {return getFilter(FZ_OFFSET, whichFilter, whichProcessor);};
                int getMxFilter(unsigned short whichFilter, short whichProcessor = 0)
                        {return getFilter(MX_OFFSET, whichFilter, whichProcessor);};
                int getMyFilter(unsigned short whichFilter, short whichProcessor = 0)
                        {return getFilter(MY_OFFSET, whichFilter, whichProcessor);};
                int getMzFilter(unsigned short whichFilter, short whichProcessor = 0)
                        {return getFilter(MZ_OFFSET, whichFilter, whichProcessor);};

                int getFilter(unsigned short whichAxis, unsigned short whichFilter, short whichProcessor = 0);

                int getFxFullScale(short whichProcessor = 0)
                        {return getFullScale(FX_OFFSET, whichProcessor);};
                int getFyFullScale(short whichProcessor = 0)
                        {return getFullScale(FY_OFFSET, whichProcessor);};
                int getFzFullScale(short whichProcessor = 0)
                        {return getFullScale(FZ_OFFSET, whichProcessor);};
                int getMxFullScale(short whichProcessor = 0)
                        {return getFullScale(MX_OFFSET, whichProcessor);};
                int getMyFullScale(short whichProcessor = 0)
                        {return getFullScale(MY_OFFSET, whichProcessor);};
                int getMzFullScale(short whichProcessor = 0)
                        {return getFullScale(MZ_OFFSET, whichProcessor);};

                int getFullScale(unsigned short whichAxis, short whichProcessor = 0);

                double getFxScaledData(unsigned short whichFilter = 3, short whichProcessor = 0)
                        { return getScaledData(FX_OFFSET, whichFilter, whichProcessor);};
                double getFyScaledData(unsigned short whichFilter = 3, short whichProcessor = 0)
                        { return getScaledData(FY_OFFSET, whichFilter, whichProcessor);};
                double getFzScaledData(unsigned short whichFilter = 3, short whichProcessor = 0)
                        { return getScaledData(FZ_OFFSET, whichFilter, whichProcessor);};
                double getMxScaledData(unsigned short whichFilter = 3, short whichProcessor = 0)
                        { return getScaledData(MX_OFFSET, whichFilter, whichProcessor);};
                double getMyScaledData(unsigned short whichFilter = 3, short whichProcessor = 0)
                        { return getScaledData(MY_OFFSET, whichFilter, whichProcessor);};
                double getMzScaledData(unsigned short whichFilter = 3, short whichProcessor = 0)
                        { return getScaledData(MZ_OFFSET, whichFilter, whichProcessor);};

                double getScaledData(unsigned short whichAxis, unsigned short whichFilter = 3, short whichProcessor = 0);

                void waitForData(unsigned int milliseconds, short whichProcessor = 0);

                std::string dataToString(double, double, double, double, double, double);


                short getProcessors() {return processors + 1;}; //returns the number of processor the PCI card has (1-4)
                void displayErrorCode(int);


                //mutators
                bool setDeviceID(string);
                int initPCI();
                void closePCI();

                void resetOffsets(short whichProcessor = 0);
                void offsetInit();

                //protected;
                bool string_to_hex(string, unsigned long &);

        private:
                unsigned long jr3ID;
                unsigned long deviceID;
                unsigned long boards;
                short processors;
                short download_code;
                short which_board;

                static unsigned short const FILTER_OFFSET;
                static unsigned short const FX_OFFSET;
                static unsigned short const FY_OFFSET;
                static unsigned short const FZ_OFFSET;
                static unsigned short const MX_OFFSET;
                static unsigned short const MY_OFFSET;
                static unsigned short const MZ_OFFSET;

        };
}

#endif