#include <string>
#include <cstdlib>              // gives size_t
#include <cctype>                       // gives isxdigit
#include <cassert>              // gives assert
#include <sstream>              // gives stringstream
#include <jr3pci_ft.h>  // gives access to JR3 PCI card. Also need jr3pci_lib.lib in project
#include <TimeDate.h>   // gives access to clock utilities.
#include <iomanip>
#include <iostream>             // gives cout
using namespace std;

#include <JR3_PCI.h>
using namespace jr3;

namespace jr3{

        unsigned short const Jr3_PCI::FILTER_OFFSET = 0x0008;
        unsigned short const Jr3_PCI::FX_OFFSET = 0X0000;
        unsigned short const Jr3_PCI::FY_OFFSET = 0X0001;
        unsigned short const Jr3_PCI::FZ_OFFSET = 0X0002;
        unsigned short const Jr3_PCI::MX_OFFSET = 0X0003;
        unsigned short const Jr3_PCI::MY_OFFSET = 0X0004;
        unsigned short const Jr3_PCI::MZ_OFFSET = 0X0005;

        /**
        * Default constructor.
        * @pre none
        * @post All members set to default values. Device will not work unless defaults are correct.
        */
        // library facilities used: string
        Jr3_PCI::Jr3_PCI(void)
        {
                jr3ID = 0x1762;
                deviceID = 0x3111;
                boards = 1;
                processors = 1;
                download_code = 1;
                which_board = 1;
        }

        /**
        * Constructor.
        * @pre deviceString is a valid hex number
        * @post All members set to values based on deviceString. Board will not work unless deviceString is correct
        */
        // library facilities used: string
        Jr3_PCI::Jr3_PCI(string deviceString)
        {
                jr3ID = 0x1762;
                string_to_hex(deviceString, deviceID);
                boards = 1;
                processors = deviceID % 16;
                download_code = 1;
                which_board = 1;
        }

        int Jr3_PCI::getFilter(unsigned short whichAxis, unsigned short whichFilter, short whichProcessor)
                /**
                * Gets the value for a particular axis from a particular filter.
                * @pre whichFilter is a valid filter number (0-6)
                * @pre whichAxis is a valid number (0-5)
                * @return value read from PCI card
                */

        {
                int returnValue;

                returnValue = static_cast<int>(read_jr3(FILTER0 + whichAxis + (whichFilter * FILTER_OFFSET), whichProcessor, which_board));
                return returnValue;
        }

        int Jr3_PCI::getFullScale(unsigned short whichAxis, short whichProcessor)
                /*  
                * Gets the factory programmed full scale for an axis from the PCI card
                * Pay attention to scale -- some are returned in odd units
                * @todo work on method to return more rational units
                * @pre whichAxis is valid (0-5)
                * @pre whichProcessor is valid
                * @return full scale in engineering units
                */
        {
                return static_cast<int>(read_jr3(FULL_S + whichAxis, whichProcessor, which_board));
        }


        double Jr3_PCI::getScaledData(unsigned short whichAxis, unsigned short whichFilter, short whichProcessor)
                /*
                * Takes reading from whichFilter and returns it in units
                * Pay attention to scale -- some are returned in odd units
                * @todo work on method to return more rational units
                * @pre whichAxis is valid (0-5)
                * @pre whichProcessor is valid
                * @return reading in engineering units
                */
        {
                double returnValue;
                const double FS_COUNTS = 16384.0;
                
                int temp1 = getFullScale(whichAxis, whichProcessor);
                int temp2 = getFilter(whichAxis, whichFilter, whichProcessor);
                returnValue = static_cast<double>(temp1 * temp2)/ FS_COUNTS;

                return returnValue;
        }


        void Jr3_PCI::waitForData(unsigned int milliseconds, short whichProcessor)
                /*
                * Waits for given number of milliseconds. If time is very short (<50 ms),
                * may consume 100% of system resources by constant reads to card. Also
                * timing will be affected by OS time slicing. Pause is a minimum value, but
                * not guaranteed to be accurate. If accuracy is required, a real time OS must be used.
                * @post system has waited at least milliseconds
                */
        {
                //TODO: debug. read_jr3 returns SIGNED value, should be unsigned. Argh
                const unsigned int WINDOWS_MIN_TIME = 45;       /* if milliseconds > than this number, use OS's delay */
                                                                                                        /* OS delay uses less system resources, but inaccurate for small values */
                                                                                                        /* Adjust this number down if accuracy not a concern */
                                                                                                        /* function will need rewrite if this is greater than 32K */
                const int MAX_COUNTS = 65536;
                const int COUNTS_PER_MS = 8;

                Time dt;
                int startCount, targetCount;


                if (milliseconds < WINDOWS_MIN_TIME) {
                        startCount = read_jr3(COUNT1, whichProcessor,  which_board) + (MAX_COUNTS / 2);
                        targetCount = (milliseconds * COUNTS_PER_MS) + startCount;
                        if (targetCount >= MAX_COUNTS) {        //handle rollover case
                                targetCount -= MAX_COUNTS;
                                while (read_jr3(COUNT1, whichProcessor,  which_board) + (MAX_COUNTS / 2) > startCount);
                        }

                        while (read_jr3(COUNT1, whichProcessor,  which_board) + (MAX_COUNTS / 2) <= targetCount);
                }
                else {
                        dt.delay(milliseconds);
                }
                return;
        }

        std::string Jr3_PCI::dataToString(double x1, double x2, double x3, double x4, double x5, double x6)
        {
                const int DECIMAL_PLACES = 2;

                string countStr = "";
                stringstream ss;

                ss << fixed << setprecision (DECIMAL_PLACES) << x1;
                countStr = ss.str() + '\t';
                ss.str("");

                ss << fixed << setprecision (DECIMAL_PLACES) << x2;
                countStr += ss.str() + '\t';
                ss.str("");

                ss << fixed << setprecision (DECIMAL_PLACES) << x3;
                countStr += ss.str() + '\t';
                ss.str("");

                ss << fixed << setprecision (DECIMAL_PLACES) << x4;
                countStr += ss.str() + '\t';
                ss.str("");

                ss << fixed << setprecision (DECIMAL_PLACES) << x5;
                countStr += ss.str() + '\t';
                ss.str("");

                ss << fixed << setprecision (DECIMAL_PLACES) << x6;
                countStr += ss.str();
                ss.str("");

                return countStr;

        }

        string Jr3_PCI::getSixChannel(short whichProcessor)
                /**
                * Gets unfiltered coupled 6 channel data directly from the sensor
                * @pre JR3 card is initialized, whichProcessor is in range 0..processors-1
                * @return Returns a comma and tab delimited string of the data in Fx, Fy, Fz, Mx, My, Mz order
                * @var short whichProcessor: processor number to access, 0 is lowest
                */
                // Library functions used: assert, jr3pci_ft.h
        {
                string countStr = "";
                stringstream ss;
                short i;
                int counts;
                assert(whichProcessor >= 0 && whichProcessor < this->processors);
                for (i = 0; i < 6; i++)
                {
                        counts = (static_cast<int>(read_jr3(RAW_C+5+(i*4), whichProcessor, which_board)));
                        ss << counts;
                        countStr += ss.str();
                        if (i != 5)
                                countStr += "\t";
                        ss.str("");

                };

                return countStr;
        }

        string Jr3_PCI::getEightChannel(short whichProcessor)
                /**
                * Gets unfiltered coupled 8 channel data directly from the sensor
                * @pre JR3 card is initialized, whichProcessor is in range 0..processors-1
                * @return Returns a comma and tab delimited string of the data
                * @var short whichProcessor: processor number to access, 0 is lowest
                */
                // Library functions used: assert, jr3pci_ft.h
        {
                string countStr = "";
                stringstream ss;
                short i;
                int counts;
                assert(whichProcessor >= 0 && whichProcessor < this->processors);
                for (i = 0; i < 8; i++)
                {
                        counts = (static_cast<int>(read_jr3(RAW_C+33+(i*4), whichProcessor, which_board)));
                        ss << counts;
                        countStr += ss.str();
                        if (i != 7)
                                countStr += "\t";
                        ss.str("");

                };

                return countStr;
        }

        void Jr3_PCI::displayErrorCode(int theError)
                /**
                *  Displays the error code associated with theError
                * @pre none
                * @post error code is displayed
                */
                // Library functions used: iostream
        {
                if (theError >= 0)
                        cout << "PCI successfully initialized." << endl;
                else if (theError == -90){
                        cout << "The .idm file needs to be in the same folder as this program." << endl;
                        cout << "Please fix and rerun the program." << endl;
                        cin.ignore();
                        cin.get();
                } else if(theError == -91){
                        cout << "Failed to open handle to Windriver ... run wdreg." << endl;
                        cout << "Please fix and rerun the program." << endl;
                        cin.ignore();
                        cin.get();
                }       else if(theError == -92){
                        cout << "Windriver version error." << endl;
                        cout << "Please fix and rerun the program." << endl;
                        cin.ignore();
                        cin.get();
                }       else if (theError == -93){
                        cout << "The device ID hardcoded in the program is incorrect." << endl;
                        cout << "Please fix and rerun the program." << endl;
                        cin.ignore();
                        cin.get();
                }  else if(theError == -94){
                        cout << "Card not in range." << endl;
                        cout << "Please fix and rerun the program." << endl;
                        cin.ignore();
                        cin.get();
                }else if (theError == -95){
                        cout << "Another program is using the PCI card." << endl;
                        cout << "Please fix and rerun the program." << endl;
                        cin.ignore();
                        cin.get();
                } else if(theError == -96){
                        cout << "Download error." << endl;
                        cout << "Please fix and rerun the program." << endl;
                        cin.ignore();
                        cin.get();
                }
                else {
                        cout << "PCI reply was " << theError << ". Unknown error." << endl;
                        cin.ignore();
                        cin.get();
                };
                return;
        }

        bool Jr3_PCI::setDeviceID(string newID)
                /**
                * Sets deviceID and processors.
                * @pre string is a valid hex device ID
                * @post deviceID and processors set. Returns true if string was a valid hex number
                * @warning Does not check if string is a valid deviceID
                */
        {
                bool isValid;
                unsigned long tempID;

                isValid = string_to_hex(newID, tempID);
                if (isValid)
                {
                        deviceID = tempID;
                        processors = tempID % 16;
                };
                return isValid;
        }

        int Jr3_PCI::getSerialNumber(short whichProcessor)
                //default value for whichProcessor = 0
        {
                return (static_cast<int>(read_jr3(SERIAL_N+0, whichProcessor,  1)));
        }

        int Jr3_PCI::getCounts(short whichAxis, short whichProcessor)
                /**
                * Gets counts for a particular axis from filter 3
                * @pre JR3 card is initialized, whichProcessor is in range 0..processors-1, whichAxis in range 0..5
                * @post Returns Fx counts
                * @var short whichProcessor: processor number to access, 0 is lowest
                * @var short whichAxis: counts to read. Fx = 0, Fy = 1, Fx = 2, Mx = 3, My = 4, Mz = 5
                * @todo clean up assert with more helpful info to user
                */
                // Library functions used: assert, jr3pci_ft.h
        {
                int counts;
                assert(whichProcessor >= 0 && whichProcessor < this->processors);
                assert(whichAxis >= 0 && whichAxis <= 5);

                counts = (static_cast<int>(read_jr3(FILTER3+whichAxis, whichProcessor, which_board)));
                return counts;
        }

        int Jr3_PCI::getClock(short whichProcessor){
                int startCount = read_jr3(COUNT1, whichProcessor,  which_board);
                return startCount;
        }

        int Jr3_PCI::getRaw(short whichProcessor, short whichAxis, int n)
        {
                int result;
                short offset;

                if (n == 6)
                        offset = 5;
                else
                        offset = 33;
                result =  (static_cast<int>(read_jr3(RAW_C+offset+(whichAxis*4), whichProcessor, 1)));
                return result;
        }


        string Jr3_PCI::getAllCounts(short whichProcessor)
                /**
                * Gets counts for all axes from filter 3
                * @pre JR3 card is initialized, whichProcessor is in range 0..processors-1, whichAxis in range 0..5
                * @post Returns all counts in formatted string with carriage return
                * @var short whichProcessor: processor number to access, 0 is lowest
                */
                // Library functions used: assert, jr3pci_ft.h
        {
                string countStr = "";
                stringstream ss;
                int i;
                assert(whichProcessor >= 0 && whichProcessor < this->processors);

                for (i = 0; i < 5; i++)
                {
                        ss << getCounts(i, whichProcessor);
                        countStr += ss.str() + '\t';
                        ss.str("");
                };
                ss << getCounts(5, whichProcessor);
                countStr += ss.str() + '\n';
                return countStr;
        }



        int Jr3_PCI::initPCI()
                /**
                * Initializes the JR3 PCI card. Must be called before using card.
                * @pre member variables have correct date for board being initialized.
                * @post JR3 card initialized. Reply code returned; >=0 is good
                */
                // Library functions used: jr3pci_ft.h
        {
                return static_cast<int>(init_jr3(jr3ID, deviceID, boards, processors, download_code, which_board));
        }

        void Jr3_PCI::closePCI()
                /**
                * Closes the JR3 PCI card.
                * @post JR3 card closed
                */
                // Library functions used: jr3pci_ft.h
        {
                close_jr3(1);
                return;
        }


        void Jr3_PCI::resetOffsets(short whichProcessor)
        /**
        * Resets offsets with value from filter2.
        * @pre JR3 card is initialized, member variables have good values
        * @post offsets reset for board 0, processor 0
        * @todo overload with abilility to specify board and processor
        */
                // Library functions used: jr3pci_ft.h
        {
                reset_offsets(whichProcessor, 1);
                return;
        }


        void Jr3_PCI::offsetInit()
                /**
                * Delays to let offsets settle, then sets offsets.
                * @pre JR3 card is initialized, member variables have good values
                * @post offsets reset for board 0, processor 0
                * @todo overload with abilility to specify board and processor
                */
                //library functions used: TimeDate.h
        {
                Time dt;
                dt.delay(500);          //500 ms delay to let offsets settle

                reset_offsets(0, 1);
                return;
        }


        bool Jr3_PCI::string_to_hex(string myString, unsigned long & myNum)
                /**
                * Converts a string that represents a hex number to an integer
                * ie 1A gives 26, 100 gives 256
                * @pre string represents a number within the bounds of positive int on the machine
                * @post Converted number in myNum; returns true if a valid hex number encountered.
                */
                // library facilities used: string, cstdlib, cctype
                // tested 15mar11 kca
        {


                bool isValid = true;
                int tempResult = 0;
                int power, exp;
                int i, j;
                char tempChar;

                i = 0;
                while (isValid && i <= static_cast<int>(myString.length()-1))
                {
                        tempChar = myString.at(i);
                        if (!isxdigit(tempChar))
                                isValid = false;
                        if (isValid)
                        {
                                power = myString.length()- static_cast<int>(i) - 1;
                                exp = 1;
                                for (j = 0; j < power; j++)
                                        exp = exp * 16;
                                if (isdigit(tempChar))
                                        tempResult += (exp * (tempChar - '0'));
                                else
                                {
                                        tempChar = toupper(tempChar);
                                        tempResult += (exp * (10 + tempChar - 'A'));
                                };
                        };
                        ++i;
                };

                if (isValid)
                        myNum = static_cast<unsigned long>(tempResult);

                return isValid;
        }
}