#ifndef JR3PCI_H

#define JR3PCI_H

#include <iostream>
#include <string>
#include <jr3pci_ft.h>
#include <stdio.h>
#include <conio.h> 

using namespace std;

class Jr3Pci
{
private:
	int reply;
	int offset;
public:
	Jr3Pci()
	{
		reply = 0;
		offset = 0;
	}

	void initSingle()
	{
		reply= init_jr3(0x1762,0x1111,1,1,1,1); // verify PCI card present, download DSP code to PCI
	}

	void initQuad()
	{
		reply= init_jr3(0x1762,0x3114,1,4,1,0); // verify PCI card present, download DSP code to PCI
	}

	void closeJr3(int boardNum)
	{
		close_jr3(boardNum);
	}

	int getReply()
	{
		return reply;
	}

	int readError(int sensorNum, int boardNum)
	{
        return read_jr3(ERRORS,sensorNum,boardNum);
	}
	int readOffsets(int sensorNum, int boardNum)
	{
		return read_jr3(OFFSETS,sensorNum,boardNum);
	}

};

#endif